// STD Libraries
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// 3rd Party Libraries
#include <SDL2/SDL.h>

void InitSetup(SDL_Window **window, SDL_Renderer **renderer, int *errno);
void LogError(char* msg, int* errno); 
int Random(int max);
void DrawRandomRectangle(SDL_Renderer *renderer);
void GameLoop(SDL_Renderer *renderer);
void Shutdown(SDL_Window *window, SDL_Renderer *renderer);

/* main function */
int main(int argc, char *argv[]) {


    // Keep a count of the errors thrown as a program return value
    int errno;

    // SDL objects for creating graphics
    SDL_Window      *window;
    SDL_Renderer    *renderer;

    InitSetup(&window, &renderer, &errno);

    GameLoop(renderer);

    Shutdown(window, renderer);

    return errno;
}

void InitSetup(SDL_Window **window, SDL_Renderer **renderer, int *errno) {
    // Seed random functions with time
    srand((int)time(NULL));

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
        LogError("Error initializing SDL2", errno);

    // Create the window and renderer
    SDL_CreateWindowAndRenderer(
        /* int width                  */ 500,
        /* int height                 */ 500,
        /* Uint32 flags               */ 0,
        /* SDL_Window **window        */ window,
        /* SDL_Renderer **renderer    */ renderer
    );

    if(!window) 
        LogError("Creation of Window Failed", errno);

    if(!renderer)
        LogError("Creation of rendere Failed", errno);
}

void Shutdown(SDL_Window *window, SDL_Renderer *renderer) {
    SDL_DestroyRenderer(/* SDL_Renderer *renderer */ renderer);
    SDL_DestroyWindow(/* SDL_Window *window */ window);

    SDL_Quit();
}

void DrawRandomRectangle(SDL_Renderer *renderer) {
    char buff[20];
    SDL_Rect rect;

    // Set a random colour and opacity for the rectangle
    SDL_SetRenderDrawColor(
        /* SDL_Renderer *renderer */ renderer,
        /* Uint8 r                */ Random(255),
        /* Uint8 g                */ Random(255),
        /* Uint8 b                */ Random(255),
        /* Uint8 a                */ Random(255)
    );                               

    rect.h = Random(100) + 20;
    rect.w = Random(100) + 20;
    rect.x = Random(500);
    rect.y = Random(500);

    // Fill a rectangle on the current renderer using the drawing colour
    SDL_RenderFillRect(
        /* SDL_Renderer *renderer */    renderer,
        /* const SDL_Rect *rect   */    &rect
    );

    // The rendering done so far has only filled up the back buffer
    // The render present sends the back buffer to the main buffer
    SDL_RenderPresent( /* SDL_Renderer *renderer */ renderer );
    
}

void GameLoop(SDL_Renderer *renderer) {
    int gameRunning = 1;
    SDL_Event event;

    while(gameRunning)
    {
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
                gameRunning = 0;
            else if(event.type == SDL_QUIT)
                gameRunning = 0;
            else if(event.type == SDL_KEYDOWN)
                DrawRandomRectangle(renderer);
        }
    }


}

void LogError(char* msg, int* errno) {
    // Print the error message and increment the error counter
    printf("%s\n", msg);
    (*errno)++;
}

int Random(int max) {
    return (rand() % max) + 1;
}
